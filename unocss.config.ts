import { defineConfig } from "@unocss/vite";
import presetsUno from "@unocss/preset-uno";
import transformerCompileClass from "@unocss/transformer-compile-class";

export default defineConfig({
	presets: [ presetsUno() ],
	transformers: [
		transformerCompileClass(),
	],
	shortcuts: {
		"flex-center": "flex justify-center items-center",
		"flex-col-center": "flex flex-col justify-center items-center",
	},
	rules: [
		[ /^wh-(\d+)$/, ([ , d ]: any) => ({ 
			width: `${d / 4}rem`,
			height: `${d / 4}rem`,
		}) ],
	],
});
